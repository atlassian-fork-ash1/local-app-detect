import { tryPorts } from './common/utils';

/**
 * Requests an image resource hosted locally at a specific port number
 *
 * @param {number} port
 * @returns {Promise}
 */
function pingServer(port) {
  return new Promise((resolve, reject) => {
    let image = new Image();
    const url = `http://127.0.0.1:${port}`;
    image.onload = resolve;
    image.onerror = reject;
    image.src = url;
  });
}

function ping(ports) {
  return tryPorts(pingServer, ports);
}

export const client = { ping };