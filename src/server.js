import http from 'http';
import { tryPorts } from './common/utils';

/**
 * Attempts to start a simple static file server that serves a
 * single pixel transparent gif, listening on a specified port
 *
 * @param port
 * @returns {Promise}
 */
function startServer(port) {
  return new Promise((resolve, reject) => {
    http.createServer((request, response) => {
      response.writeHead(200, {
        'Content-Type': 'image/gif'
      });
      // send 1px transparent gif
      response.end('R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7', 'base64');
    })
      .once('error', (err) => {
        if (err.code === 'EADDRINUSE') {
          reject(err);
        }
      })
      .once('listening', resolve)
      .listen(port, '127.0.0.1');
  });
}

function start(ports) {
  return tryPorts(startServer, ports);
}

export const server = { start };