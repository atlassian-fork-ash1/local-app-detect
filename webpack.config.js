const serverConfig = {
  target: 'node',
  entry: './src/server.js',
  output: {
    filename: 'server.js',
    path: __dirname,
    library: ['[name]'],
    libraryTarget: 'umd'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
};

const clientConfig = {
  entry: './src/client.js',
  output: {
    filename: 'client.js',
    path: __dirname,
    library: ['[name]'],
    libraryTarget: 'umd'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
};

module.exports = [ clientConfig, serverConfig ];