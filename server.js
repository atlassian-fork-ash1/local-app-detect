(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["main"] = factory();
	else
		root["main"] = factory();
})(global, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading wasm modules
/******/ 	var installedWasmModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// object with all compiled WebAssembly.Modules
/******/ 	__webpack_require__.w = {};
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/common/config.js":
/*!******************************!*\
  !*** ./src/common/config.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nvar ports = exports.ports = Object.freeze([22274, 22300, 22304, 31417, 31456]);\n\n//# sourceURL=webpack://%5Bname%5D/./src/common/config.js?");

/***/ }),

/***/ "./src/common/utils.js":
/*!*****************************!*\
  !*** ./src/common/utils.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.tryPorts = tryPorts;\n\nvar _config = __webpack_require__(/*! ./config */ \"./src/common/config.js\");\n\nfunction _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }\n\n/**\n * Iterates through an array of port numbers attempting to execute\n * a function fn passing each port number an argument. Returns either the\n * successful port number or the error encountered.\n *\n * @param {function<Promise>} fn\n * @param {Array<number>} portNumbers\n * @returns {Promise<number|Error>}\n */\nfunction tryPorts(fn) {\n  var portNumbers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _config.ports;\n\n  try {\n    var _portNumbers = _toArray(portNumbers),\n        port = _portNumbers[0],\n        others = _portNumbers.slice(1);\n\n    return fn(port).then(function () {\n      return port;\n    }).catch(function (e) {\n      if (others.length) {\n        return tryPorts(fn, others);\n      }\n      return Promise.reject(e);\n    });\n  } catch (e) {\n    // do nothing\n  }\n}\n\n//# sourceURL=webpack://%5Bname%5D/./src/common/utils.js?");

/***/ }),

/***/ "./src/server.js":
/*!***********************!*\
  !*** ./src/server.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.server = undefined;\n\nvar _http = __webpack_require__(/*! http */ \"http\");\n\nvar _http2 = _interopRequireDefault(_http);\n\nvar _utils = __webpack_require__(/*! ./common/utils */ \"./src/common/utils.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/**\n * Attempts to start a simple static file server that serves a\n * single pixel transparent gif, listening on a specified port\n *\n * @param port\n * @returns {Promise}\n */\nfunction startServer(port) {\n  return new Promise(function (resolve, reject) {\n    _http2.default.createServer(function (request, response) {\n      response.writeHead(200, {\n        'Content-Type': 'image/gif'\n      });\n      // send 1px transparent gif\n      response.end('R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7', 'base64');\n    }).once('error', function (err) {\n      if (err.code === 'EADDRINUSE') {\n        reject(err);\n      }\n    }).once('listening', resolve).listen(port, '127.0.0.1');\n  });\n}\n\nfunction start(ports) {\n  return (0, _utils.tryPorts)(startServer, ports);\n}\n\nvar server = exports.server = { start: start };\n\n//# sourceURL=webpack://%5Bname%5D/./src/server.js?");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"http\");\n\n//# sourceURL=webpack://%5Bname%5D/external_%22http%22?");

/***/ })

/******/ });
});